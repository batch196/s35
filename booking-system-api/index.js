const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

/*
	mongoose connection
*/
// replace password in the connection string
//add the database name before the ? in the connection string

mongoose.connect("mongodb+srv://admin:admin123@cluster0.ynbqcrk.mongodb.net/bookingAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection; //create a connection ti the db if success or fail
db.on('error',console.error.bind(console,"MongoDB Connection Error"));
db.once('open',()=>console.log("Connected to MongoDB"))




//to be able to handle the request body and parse it into object
app.use(express.json())



//import our route and use it as middleware
//which means we will be able to group together our routes
const courseRoutes = require('./routes/courseRoutes');
// console.log(courseRoutes);
app.use('/courses',courseRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);






app.listen(port,() => console.log(`Express API running at port 4000`))



