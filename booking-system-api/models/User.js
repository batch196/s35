
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type:String,
		required:[true,"First name is required"]
	},
	lastName: {
		type:String,
		required:[true,"Last name is required"]
	},
	email: {
		type:String,
		required:[true,"email is required"]
	},
	password: {
		type:String,
		required: [true,"Enter password"]
	},
	isAdmin: {
		type:Boolean,
		default: false
	},
	mobileNumber: {
		type:String,
		required: [true,"Mobile number is required"]
	},
	enrolees: [

		{
			courseId: {
				type:String,
				required: [true,"Course Id is required"]
			},
			enrolledOn: {
				type:Date,
				default: new Date()
			},
			status: {
				type:String,
				default: "Enrolled"
			}
		}

	]
})

module.exports = mongoose.model("User",userSchema);
