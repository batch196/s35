



const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt")
const User = require("../models/User")

router.post("/",(req,res)=>{
	// console.log(req.body)

	//using bcrypt, hide the user's password to hide in incrypted display
	const hashedPw = bcrypt.hashSync(req.body.password,10)
	console.log(hashedPw);


	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNumber: req.body.mobileNumber
	})
	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
})



//POST method route (activity)
router.post("/details",(req,res)=>{
	// console.log(req.body);

	User.find(req.body)
	.then(result => res.send(result))
	.catch(error => res.send(error))

})

module.exports = router;