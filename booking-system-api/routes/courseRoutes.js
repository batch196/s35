//the roueter method allows us to contain our routes


const express = require("express");
const router = express.Router();

//import course model so we can manupulate it and add a new course

const Course = require("../models/Course");

router.get('/',(req,res) =>{
	// res.send("This route will get all course documents")
	//to be able to query our collection we use the model connected to that collection

	//Model.find()
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
});

router.post('/',(req,res)=>{
	// res.send("This route will create a new course document")

	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})
	// console.log(newCourse);

	newCourse.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
});

module.exports = router;








